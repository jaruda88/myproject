from flask import Flask
from views.main import main

app = Flask(__name__)
app.register_blueprint(main.bp, url_prefix='/main')

#@app.route('/')
def create_app():
    return app

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5040, debug=False)


